import xs from 'xstream';
import R from 'ramda';

const isBetween = R.curry((low, high, value) => {
    const atLeast = !low || low <= value;
    const atMost = !high || high >= value;

    return atLeast && atMost;
});

function getFilters(filterBy) {
    const filterCriteria = [R.identity];
    const keys = Object.keys(filterBy);

    if (keys.includes('group')) {
        filterCriteria.push(R.propEq('group', filterBy.group));
    }
    if (keys.includes('dateRange')) {
        const [low, high] = filterBy.dateRange;
        const isInDateRange = isBetween(low)(high);
        const isModifiedMatcher = R.propSatisfies(
            isInDateRange,
            'modifiedOn'
        );
        const isCreatedMatcher = R.propSatisfies(
            isInDateRange,
            'createdOn'
        );
        const isModifiedOrCreatedInDateRange = R.anyPass([
            isModifiedMatcher,
            isCreatedMatcher
        ]);

        filterCriteria.push(isModifiedOrCreatedInDateRange);
    }

    const filters = filterCriteria.map(crit => R.filter(crit));

    return R.compose.apply(null, filters);
}

export default class {
    constructor() {
        this.notes = [];
        this.addNote = this.addNote.bind(this);
        this.changeEventQueue = [];

        const signal = {
            start: (observer) => {
                this.observer = observer;
                this.changeEventQueue.forEach((event) => {
                    this.observer.next(event);
                });

                this.changeEventQueue = [];
            },
            stop: () => {
                this.observer.complete();
            }
        }

        this.onChange$ = xs.create(signal);
    }

    emitChange() {
        if (this.observer) {
            this.observer.next();
        } else {
            this.changeEventQueue.push(undefined);
        }
    }

    addNote(note) {
        this.notes.push(note);
        this.emitChange();
    }
    getNotes(filterBy = {}) {
        const filter = getFilters(filterBy);

        const notes = filter(this.notes);
        const topics = R.compose(
            R.pluck('topic'),
            R.uniq
        )(notes);

        return {
            topics,
            notes
        };
    }
    get change() {
        return this.onChange$;
    }
};
