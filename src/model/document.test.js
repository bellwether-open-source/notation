import {expect} from 'chai';
import Chance from 'chance';
import R from 'ramda'
import uuid from 'uuid';

import Document from './document';

const chance = new Chance();

function createNote() {
    return {};
}

describe('Feature: Document', () => {
    it('Scenario: Document listing when document has no contents', () => {
        // given a document
        const document = new Document();

        // when general listing requested
        const listing = document.getNotes();

        // it should return a listing
        expect(listing).not.to.equal(undefined);

        // it should have an empty list of notes
        expect(listing.notes).to.deep.equal([]);
    });

    it('Scenario: Document listing after notes added', () => {
        // setup
        const numberOfNotes = chance.integer({min: 1, max: 100});
        const numberOfTopics = chance.integer({min: 1, max: numberOfNotes});

        // given a document
        const document = new Document();

        // given notes
        const notes = Array(numberOfNotes).fill().map(createNote);

        // given notes have topics
        const topics = chance.n(chance.string, numberOfTopics);

        notes.forEach((note, index) => {
            const topic = topics[index % topics.length];

            note.topic = topic;
        });

        // when notes added to document
        notes.forEach(document.addNote);

        // when general listing requested
        const listing = document.getNotes();

        // then it should enumerate all topics
        expect(listing.topics, 'topics').to.deep.equal(topics);

        // then it should provide index of notes
        expect(listing.notes, 'notes').to.deep.equal(notes);

        // then change event should fire for every added note
        let changeCount = 0;
        const subscription = document.change.subscribe({
            next() {
                changeCount += 1;
            }
        });

        return new Promise((resolve) => {
            const UGLY_MS_TIMEOUT_FOR_ASYNC_CODE = 100;

            setTimeout(() => {
                expect(changeCount).to.equal(numberOfNotes);
                resolve();
            }, UGLY_MS_TIMEOUT_FOR_ASYNC_CODE);
        });
    });

    it('Scenario: Change subscription prior to adding notes', () => {
        // setup
        const numberOfNotes = chance.integer({min: 1, max: 100});
        const numberOfTopics = chance.integer({min: 1, max: numberOfNotes});

        // given a document
        const document = new Document();

        // given a subscription
        let changeCount = 0;
        const subscription = document.change.subscribe({
            next() {
                changeCount += 1;
            }
        });

        // given notes
        const notes = Array(numberOfNotes).fill().map(createNote);

        // when notes added to document
        notes.forEach(document.addNote);

        // then change event should fire for every added note
        return new Promise((resolve) => {
            const UGLY_MS_TIMEOUT_FOR_ASYNC_CODE = 100;

            setTimeout(() => {
                expect(changeCount).to.equal(numberOfNotes);
                resolve();
            }, UGLY_MS_TIMEOUT_FOR_ASYNC_CODE);
        });
    });

    it('Scenario: Document listing by group', () => {
        // setup
        const numberOfNotes = chance.integer({min: 10, max: 100});
        const numberOfNotesToAddGroupTo = chance.integer({min: 1, max: numberOfNotes - 1});

        // given a document
        const document = new Document();

        // given notes with topics
        const topics = chance.n(chance.string, numberOfNotes);
        const notes = Array(numberOfNotes)
            .fill()
            .map((unused, index) => {
                const topic = topics[index];

                return {topic};
            });

        // given group
        const group = chance.sentence();

        // given notes on document
        notes.forEach(document.addNote);

        // given some notes have the group
        const groupedNotes = chance.pickset(notes, numberOfNotesToAddGroupTo);

        groupedNotes.forEach((note) => {
            note.group = group;
        });

        // when group listing requested
        const listing = document.getNotes({group});

        // then it should enumerate all topics within the group
        const expectedTopics = groupedNotes.map(({topic}) => topic).sort();
        const actualTopics = [...listing.topics].sort();

        expect(actualTopics, 'topics').to.deep.equal(expectedTopics);

        // then it should provide index of notes within the group
        const sortByTopic = R.sortBy(R.prop('topic'));
        const actualNotes = sortByTopic(listing.notes);
        const expectedNotes = sortByTopic(groupedNotes);

        expect(actualNotes, 'notes').to.deep.equal(expectedNotes);
    });

    it('Scenario: Document listing by date range', () => {
        // setup
        const numberOfNotes = chance.integer({min: 10, max: 100});
        const numberOfMatches = chance.integer({min: 5, max: numberOfNotes - 1});

        // given a document
        const document = new Document();

        // given a date range
        const dayAsMillis = 24 * 3600 * 1000;
        const now = new Date().getTime();
        const rangeLow = new Date(now - dayAsMillis);
        const rangeHigh = new Date(now + dayAsMillis);
        const dateRange = [rangeLow, rangeHigh];

        // given notes
        const notes = Array(numberOfNotes)
            .fill()
            .map((unused, index) => {
                const identifier = uuid.v4();

                return {identifier};
            });

        // given some notes modified in specified range
        const [matchingNotes, others] = R.splitAt(numberOfMatches, notes);
        const [matchByCreation, matchByModify] = R.splitAt(
            Math.trunc(matchingNotes.length / 2)
        )(matchingNotes);
        matchByCreation.forEach((note) => {
            note.modifiedOn = 0;
            note.createdOn = now;
        });
        matchByModify.forEach((note) => {
            note.modifiedOn = now;
            note.createdOn = 0;
        });
        others.forEach((note) => {
            note.modifiedOn = 0;
            note.createdOn = 0;
        });

        // when notes added to document
        notes.forEach(document.addNote);

        // when search by date range performed
        const listing = document.getNotes({dateRange});

        // then it should index specified matching documents
        const sortByIdentifier = R.sortBy(R.prop('identifier'));

        const listingNotes = sortByIdentifier(listing.notes)
        const expectedNotes = sortByIdentifier(matchingNotes);

        expect(listingNotes).to.deep.equal(expectedNotes);
    });

    it('Scenario: Document listing by date range (low)', () => {
        // setup
        const numberOfNotes = chance.integer({min: 10, max: 100});
        const numberOfMatches = chance.integer({min: 5, max: numberOfNotes - 1});

        // given a document
        const document = new Document();

        // given a date range with only lower bound
        const dayAsMillis = 24 * 3600 * 1000;
        const now = new Date().getTime();
        const outOfBounds = 0;
        const rangeLow = new Date(now - dayAsMillis);
        const rangeHigh = undefined;
        const dateRange = [rangeLow, rangeHigh];

        // given notes
        const notes = Array(numberOfNotes)
            .fill()
            .map((unused, index) => {
                const identifier = uuid.v4();

                return {identifier};
            });

        // given some notes modified in specified range
        const [matchingNotes, others] = R.splitAt(numberOfMatches, notes);
        const [matchByCreation, matchByModify] = R.splitAt(
            Math.trunc(matchingNotes.length / 2)
        )(matchingNotes);
        matchByCreation.forEach((note) => {
            note.modifiedOn = outOfBounds;
            note.createdOn = now;
        });
        matchByModify.forEach((note) => {
            note.modifiedOn = now;
            note.createdOn = outOfBounds;
        });
        others.forEach((note) => {
            note.modifiedOn = outOfBounds;
            note.createdOn = outOfBounds;
        });

        // when notes added to document
        notes.forEach(document.addNote);

        // when search by date range performed
        const listing = document.getNotes({dateRange});

        // then it should index specified matching documents
        const sortByIdentifier = R.sortBy(R.prop('identifier'));

        const listingNotes = sortByIdentifier(listing.notes)
        const expectedNotes = sortByIdentifier(matchingNotes);

        expect(listingNotes).to.deep.equal(expectedNotes);
    });

    it('Scenario: Document listing by date range (high)', () => {
        // setup
        const numberOfNotes = chance.integer({min: 10, max: 100});
        const numberOfMatches = chance.integer({min: 5, max: numberOfNotes - 1});

        // given a document
        const document = new Document();

        // given a date range with only upper bound
        const dayAsMillis = 24 * 3600 * 1000;
        const now = new Date().getTime();
        const outOfBounds = new Date(now + (2 * dayAsMillis));
        const rangeLow = undefined;
        const rangeHigh = new Date(now + dayAsMillis);
        const dateRange = [rangeLow, rangeHigh];

        // given notes
        const notes = Array(numberOfNotes)
            .fill()
            .map((unused, index) => {
                const identifier = uuid.v4();

                return {identifier};
            });

        // given some notes modified in specified range
        const [matchingNotes, others] = R.splitAt(numberOfMatches, notes);
        const [matchByCreation, matchByModify] = R.splitAt(
            Math.trunc(matchingNotes.length / 2)
        )(matchingNotes);
        matchByCreation.forEach((note) => {
            note.modifiedOn = outOfBounds;
            note.createdOn = now;
        });
        matchByModify.forEach((note) => {
            note.modifiedOn = now;
            note.createdOn = outOfBounds;
        });
        others.forEach((note) => {
            note.modifiedOn = outOfBounds;
            note.createdOn = outOfBounds;
        });

        // when notes added to document
        notes.forEach(document.addNote);

        // when search by date range performed
        const listing = document.getNotes({dateRange});

        // then it should index specified matching documents
        const sortByIdentifier = R.sortBy(R.prop('identifier'));

        const listingNotes = sortByIdentifier(listing.notes)
        const expectedNotes = sortByIdentifier(matchingNotes);

        expect(listingNotes).to.deep.equal(expectedNotes);
    });

    it.skip('Scenario: Search notes by specific reference', () => {
        // given a document
        // given a reference
        // given document has notes
        // given subset of notes has specified reference
        // when search by specific reference performed
        // then document should provide listing of matching notes
    });
});

