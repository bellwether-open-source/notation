import {expect} from 'chai';
import Note from './note';
import Chance from 'chance';
import R from 'ramda';

import {Reference, Factory as ReferenceFactory} from './reference';

const chance = new Chance();

const TEST_EXECUTION_TIME_THRESHOLD_MILLISECONDS = 1000;

function createReferenceDetails() {
    return {
        name: chance.sentence(),
        location: chance.string()
    };
}

describe('Feature: Note', () => {
    it('Scenario: Note creation', () => {
        // when a note is created
        const note = new Note();

        // then it should receive timestamp associated with creation (now)
        const referenceDate = new Date().getTime();

        expect(note.createdOn).to.be.instanceOf(Date);

        const createdOn = note.createdOn.getTime();

        expect(createdOn - referenceDate)
            .to.be.below(TEST_EXECUTION_TIME_THRESHOLD_MILLISECONDS);

        // then it should not have a modification timestamp
        expect(note.modifiedOn).to.equal(undefined);

        // then it should not have text
        expect(note.text).to.equal('');

        // then it should not have title
        expect(note.title).to.equal('');

        // then it should have no references
        expect(note.references).to.deep.equal([]);

        // then it should not be valid
        expect(note.valid).to.equal(false);
    });

    it('Scenario: Note modification (text)', () => {
        // given a note
        const note = new Note();

        // when the note's text is modified
        const updatedText = chance.string();

        note.text = updatedText;

        // then it should receive timestamp associated with creation (now)
        const referenceDate = new Date().getTime();

        expect(note.modifiedOn).to.be.instanceOf(Date);

        const modifiedOn = note.createdOn.getTime();
        expect(modifiedOn - referenceDate)
            .to.be.below(TEST_EXECUTION_TIME_THRESHOLD_MILLISECONDS);

        // then it should reflect updated text
        expect(note.text).to.equal(updatedText);

        // then it should be valid
        expect(note.valid).to.equal(true);
    });

    it('Scenario: Note contains no text', () => {
        // given a note
        const note = new Note();

        // given note has text
        note.text = chance.string();

        // when the note's text is set to empty
        note.text = '';

        // then it should not be valid
        expect(note.valid).to.equal(false);
    });

    it('Scenario: Note modification (title)', () => {
        // given a note
        const note = new Note({
            text: chance.string(),
            createdOn: new Date(0),
            modifiedOn: new Date(0),
            topics: [],
            groups: []
        });

        // when the note's title is modified
        const updatedTitle = chance.string();

        note.title = updatedTitle;

        // then it should receive timestamp associated with creation (now)
        const referenceDate = new Date().getTime();

        expect(note.modifiedOn).to.be.instanceOf(Date);

        const modifiedOn = note.createdOn.getTime();
        expect(modifiedOn - referenceDate)
            .to.be.below(TEST_EXECUTION_TIME_THRESHOLD_MILLISECONDS);

        // then it should reflect updated title
        expect(note.title).to.equal(updatedTitle);
    });


    it('Scenario: Note contains only whitespace', () => {
        // given a note
        const note = new Note();

        // when the note's text is set to contain only whitespace
        note.text = ' \t \n';

        // then it should not be valid
        expect(note.valid).to.equal(false);
    });

    it('Scenario: Note deserialization', () => {
        // given an object describing a valid note
        const noteDetails = {
            createdOn: chance.date(),
            modifiedOn: chance.date(),
            text: chance.string(),
            topics: chance.n(chance.word, 10),
            groups: chance.n(chance.word, 10)
        };

        // when the object is provided as a constructor parameter
        const note = new Note(noteDetails);

        // then a note should be constructed with the same values as the object
        expect(note.createdOn).to.equal(noteDetails.createdOn);
        expect(note.modifiedOn).to.equal(noteDetails.modifiedOn);
        expect(note.text).to.equal(noteDetails.text);

        expect(note.topics).to.deep.equal(noteDetails.topics);
        expect(R.identical(note.topics, noteDetails.topics)).to.equal(false);

        expect(note.groups).to.deep.equal(noteDetails.groups);
        expect(R.identical(note.groups, noteDetails.groups)).to.equal(false);
    });

    it('Scenario: Note given references', () => {
        const numberOfReferences = chance.integer({min: 1, max: 10});

        // given a note
        const note = new Note();

        // when references added to note
        const references = chance.n(createReferenceDetails, numberOfReferences)
            .map(ReferenceFactory)

        references.forEach(ref => note.references.push(ref));

        // then note should have added references
        expect(note.references).to.deep.equal(references);
    });
});
