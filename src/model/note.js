export default class {
    constructor(details) {
        if (details) {
            this._createdOn = details.createdOn;
            this._modifiedOn = details.modifiedOn;
            this._text = details.text;
            this._topics = [...details.topics];
            this._groups = [...details.groups];
        } else {
            this._createdOn = new Date();
            this._text = '';
            this._title = '';
            this._references = [];
        }
    }

    get createdOn() {
        return this._createdOn;
    }

    get modifiedOn() {
        return this._modifiedOn;
    }

    get valid() {
        return this._text.trim() !== '';
    }

    get text() {
        return this._text;
    }

    set text(value) {
        this._text = value;
        this._modifiedOn = new Date();
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    get topics() {
        return this._topics;
    }

    get groups() {
        return this._groups;
    }

    get references() {
        return this._references;
    }
}
