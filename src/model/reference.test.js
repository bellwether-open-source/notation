import {expect} from 'chai';
import Chance from 'chance';
import {Reference, BibleReference, Factory} from './reference';
import R from 'ramda';

const chance = new Chance();

function createBibleChapter(book, chapter) {
    return `${book} ${chapter}`;
}

function createBibleVerse(book, chapter, verse) {
    return `${createBibleChapter(book, chapter)}:${verse}`;
}

describe('Feature: Reference', () => {
    it('Scenario: Reference type existence', () => {
        // given a random source with name and location
        const source = {
            name: chance.string(),
            location: chance.string()
        };

        // when reference factory given location details
        const reference = Factory(source);

        // then it should instantiate a Reference
        expect(reference).to.be.instanceOf(Reference);

        // then the returned Reference should have a Source
        expect(reference.Source).not.to.equal(undefined);

        // then the Source should have a Name matching source name
        expect(reference.Source.Name).to.equal(source.name);

        // then the Source should have a Location matching source location
        expect(reference.Source.Location).to.equal(source.location);
    });

    it('Scenario: Reference is bible-verse-centric', () => {
        const makeIntegerString = () => String(chance.natural());

        // given bible verse location
        const book = chance.word();
        const chapter = makeIntegerString();
        const verse = makeIntegerString();
        const source = {
            bibleVerse: createBibleVerse(book, chapter, verse)
        };

        // when reference factory given location details
        const reference = Factory(source);

        // then it should instantiate a BibleReference
        expect(reference).to.be.instanceOf(BibleReference)
            .and.to.be.instanceOf(Reference);

        // then the reference Source should have a Book, Chapter, Verse
        const {Book, Chapter, Verse} = reference.Source;

        expect(Book).to.equal(book);
        expect(Chapter).to.equal(chapter);
        expect(Verse).to.equal(verse);
    });

    it('Scenario: Reference is bible-chapter-centric', () => {
        const makeIntegerString = () => String(chance.natural());

        // given bible verse location
        const book = chance.word();
        const chapter = makeIntegerString();
        const source = {
            bibleVerse: createBibleChapter(book, chapter)
        };

        // when reference factory given location details
        const reference = Factory(source);

        // then the reference Source should have a Book, Chapter
        const {Book, Chapter} = reference.Source;

        expect(Book).to.equal(book);
        expect(Chapter).to.equal(chapter);
    });

    it('Scenario: Reference is bible-book-centric', () => {
        const makeIntegerString = () => String(chance.natural());

        // given bible verse location
        const book = chance.word();
        const source = {
            bibleVerse: book
        };

        // when reference factory given location details
        const reference = Factory(source);

        // then the reference Source should have a Book, Chapter
        const {Book} = reference.Source;

        expect(Book).to.equal(book);
    });
});

