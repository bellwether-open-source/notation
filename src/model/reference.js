import R from 'ramda';

export class Reference {
    constructor({name, location}) {
        this._source = {
            name,
            location
        };
    }

    get Source() {
        return {
            Name: this._source.name,
            Location: this._source.location
        }
    }
}

const parseVerseReference = location => R
    .match(/(\w+) (\d+):(\d+)/, location)
    .slice(1, 4);
const parseChapterReference = location => R
    .match(/(\w+) (\d+)/, location)
    .slice(1, 3);
const parseBookReference = location => R
    .match(/(\w+)/, location)
    .slice(1, 2);

const isReferenceMatch = x => x.length > 0;

const parseReference = location => [
        parseVerseReference,
        parseChapterReference,
        parseBookReference
    ]
    .map(matcher => matcher(location))
    .find(isReferenceMatch);

export class BibleReference extends Reference {
    constructor({bibleVerse}) {
        super({});

        const [book, chapter, verse] = parseReference(bibleVerse);

        this._bibleVerse = {
            book,
            chapter,
            verse
        };
    }

    get Source() {
        return {
            Book: this._bibleVerse.book,
            Chapter: this._bibleVerse.chapter,
            Verse: this._bibleVerse.verse
        }
    }
}

export const Factory = R.cond([
    [R.prop('bibleVerse'), details => new BibleReference(details)],
    [R.T, details => new Reference(details)]
]);

